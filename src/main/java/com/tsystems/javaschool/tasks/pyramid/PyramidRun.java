package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.List;

public class PyramidRun {
    public static void main(String[] args) {
        PyramidBuilder p = new PyramidBuilder();
        List<Integer> inputNumbers = Arrays.asList(1,2,3,6,12,8,23,78,14,7,25,67,89,2,56,12,14);
        p.buildPyramid(inputNumbers);
    }
}