package com.tsystems.javaschool.tasks.subsequence;

import java.util.Arrays;

public class SubsequenceRun extends Subsequence {
    public static void main(String[] args) {
        SubsequenceRun s = new SubsequenceRun();
        boolean b = s.find(Arrays.asList("A", "B", "C", "D"),
                Arrays.asList("BD", "A", "ABC", "B", "M", "D", "M","C", "DC", "D"));
        System.out.println(b); // Result: true
    }
}
