package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.Iterator;

public class Subsequence {
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) throw new IllegalArgumentException();
        Iterator<Object> iterx = x.iterator();
        Iterator<Object> itery = y.iterator();
        int count = 0;
        while (iterx.hasNext()){
            Object currentx = iterx.next();
            while (itery.hasNext()){
                Object currenty = itery.next();
                if (currentx.equals(currenty)){
                    if (iterx.hasNext()) currentx = iterx.next();
                    count++;
                }
            }
        }
        return x.size() == count;
    }
}
