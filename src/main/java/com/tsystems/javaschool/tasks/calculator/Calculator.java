package com.tsystems.javaschool.tasks.calculator;
import java.text.DecimalFormat;
import java.util.LinkedList;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        double result = 0;
        String polishNotation = getPolishNotation(statement);
        LinkedList<Double> operands = new LinkedList<>();

        if (polishNotation == null || polishNotation.equals("")) return null;

        for (int i = 0; i < polishNotation.length(); i++) {
            if (Character.isDigit(polishNotation.charAt(i))){
                StringBuilder temp = new StringBuilder("");
                while (Character.isDigit(polishNotation.charAt(i)) || polishNotation.charAt(i) == '.') {
                    temp.append(polishNotation.charAt(i));
                    i++;
                    if (i == polishNotation.length()) break;
                }
                operands.add(Double.parseDouble(temp.toString()));
            }

            if (isOperator(polishNotation.charAt(i))){
                if (operands.size() < 2) return null;
                double a = operands.removeLast();
                double b = operands.removeLast();

                switch (polishNotation.charAt(i)){
                    case '+': result = b + a; break;
                    case '-': result = b - a; break;
                    case '*': result = b * a; break;
                    case '/':{
                        if (a == 0) return null;
                        result = b / a;
                    } break;
                }
                operands.add(result);
            }
        }
        if (operands.size() > 1) return null;
        return roundNumber(operands.getLast());
    }

    private String getPolishNotation (String statement){
        StringBuilder result = new StringBuilder("");
        LinkedList<Character> operators = new LinkedList<>();

        if (statement == null || statement.equals("")) return null;
        int countOpenBrackets = 0;
        int countCloseBrackets = 0;

        for (int i = 0; i < statement.length(); i++) {
            char c = statement.charAt(i);

            if (isDelimiter(c)){
                continue;
            }

            else if (Character.isDigit(statement.charAt(i))){
                int dotCounter = 0;
                while (Character.isDigit(statement.charAt(i)) || statement.charAt(i) == '.') {
                    result.append(statement.charAt(i));
                    i++;
                    if (i == statement.length()) break;
                    if (statement.charAt(i) == '.') {
                        dotCounter++;
                        if (dotCounter == 2) return null;

                    }
                }
                result.append(' ');
                i--;
            }

            else if (c == '('){
                operators.add(c);
                countOpenBrackets++;
            }

            else if (c == ')'){
                while (operators.getLast() != '('){
                    result.append(operators.removeLast());
                    result.append(' ');
                    if (operators.isEmpty()) return null;
                }
                operators.removeLast();
                countCloseBrackets++;
            }

            else if (isOperator(c)){
                while (!operators.isEmpty() &&
                        getPriority(c) <= getPriority(operators.getLast())){
                    result.append(operators.removeLast());
                    result.append(' ');
                }
                operators.add(c);
            }
            else return null;
        }

        if (countOpenBrackets != countCloseBrackets) return null;

        while (!operators.isEmpty()){
            result.append(operators.removeLast());
            result.append(' ');
        }

        return result.toString();
    }
    private static boolean isDelimiter (char c){
        return (c == ' ');
    }

    private static boolean isOperator (char c){
        return (c == '+' || c == '-' || c == '*' || c == '/');
    }

    private static int getPriority (char c){
        if ((c == '+') || (c == '-')){
            return 0;
        }
        if ((c == '*') || (c == '/')){
            return 1;
        }
        return -1;
    }

    private static String roundNumber (double number){
        String pattern = "##.####";
        DecimalFormat decimalFormat = new DecimalFormat(pattern);
        String format = decimalFormat.format(number);
        return format;
    }
}
