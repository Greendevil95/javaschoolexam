
The exam includes 3 tasks to be done: [Calculator](/tasks/Calculator.md), [Pyramid](/tasks/Pyramid.md), and 
[Subsequence](/tasks/Subsequence.md)

### Result ###

* Author name : {Denis Bondarev}
* Codeship : [ ![Codeship Status for Greendevil95/javaschoolexam](https://app.codeship.com/projects/44da90f0-9ae8-0136-cc78-3a6df96c6020/status?branch=master)](https://app.codeship.com/projects/305703)

